package com.practice.finalProject.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.practice.finalProject.services.XmlFileServiceInterface;

import jakarta.xml.bind.JAXBException;

@RestController
@RequestMapping("/api")
public class XmlController {
    
    @Autowired
    private XmlFileServiceInterface service;

    @PostMapping("/upload")
    public ResponseEntity<String> saveFile(@RequestParam("file") MultipartFile multipartFile) throws IOException{

        service.saveFile(multipartFile);
        return ResponseEntity.ok("working");
    }


    @GetMapping(value = "/games/{fileName}")
    public ResponseEntity<Object> getJavaObject(@PathVariable("fileName") String fileName) throws IOException, JAXBException {

        Object gameexplorer = service.XmltoJava(fileName);

        System.out.println("\n\n"+ gameexplorer);
        
        // return ResponseEntity.ok(gameexplorer);
        return new ResponseEntity<Object>(gameexplorer, HttpStatus.OK);
    }

}
