package com.practice.finalProject.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import com.practice.finalProject.models.XmlFile;
import com.practice.finalProject.models.gameExplorer;
import com.practice.finalProject.repositories.XmlFileRepository;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;



@Service
public class XmlFileService implements XmlFileServiceInterface{

    @Autowired
    private XmlFileRepository repo;

    @Override
    public void saveFile(MultipartFile multipartFile) throws IOException {
        
        if(multipartFile.isEmpty()){
            throw new IllegalArgumentException("File is empty");
        }
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        XmlFile xmlFile = new XmlFile();
        xmlFile.setName(fileName);
        xmlFile.setContent(multipartFile.getBytes());
        repo.save(xmlFile);
    }
    

    @Override
    public Object XmltoJava(String fileName) throws JAXBException, FileNotFoundException {

        // finding the xml file in database using the name of file
        Optional<XmlFile> xmlFile = repo.findByName(fileName);

        if (!xmlFile.isPresent()) {
            throw new FileNotFoundException("File " + fileName + " not found.");
        }

        // accessing the data of the file
        byte[] xmlData = xmlFile.get().getContent();

        // converting the data of the file into string
        String xmlString = new String(xmlData, StandardCharsets.UTF_8);
        
        JAXBContext jaxbContext = JAXBContext.newInstance(gameExplorer.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(xmlString);
        gameExplorer gameexplorer = (gameExplorer) unmarshaller.unmarshal(reader);

        return gameexplorer;
    }
    
}
