package com.practice.finalProject.services;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import jakarta.xml.bind.JAXBException;

public interface XmlFileServiceInterface {

    void saveFile(MultipartFile multipartFile) throws IOException;

    Object XmltoJava(String fileName) throws JAXBException, FileNotFoundException;
    
}
