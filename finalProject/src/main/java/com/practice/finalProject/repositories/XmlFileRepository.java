package com.practice.finalProject.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practice.finalProject.models.XmlFile;

public interface XmlFileRepository extends JpaRepository<XmlFile, Integer>{

    Optional<XmlFile> findByName(String fileName);
    
}
