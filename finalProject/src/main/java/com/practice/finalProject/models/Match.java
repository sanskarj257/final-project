package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Match {

    @XmlAttribute(name = "type")
    private String type;

    @XmlAttribute(name = "value")
    private String value;

    @XmlElement
    private int count;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "Match [type=" + type + ", value=" + value + ", count=" + count);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}
