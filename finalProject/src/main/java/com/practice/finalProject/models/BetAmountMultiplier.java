package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class BetAmountMultiplier {

    @XmlAttribute(name = "default")
    private String defaultMultiplierVar;

    @XmlElement(name = "multiplier")
    private List<String> multipliersList;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    public String getDefaultMultiplier() {
        return defaultMultiplierVar;
    }

    public void setDefaultMultiplier(String defaultMultiplier) {
        this.defaultMultiplierVar = defaultMultiplier;
    }

    public List<String> getMultipliers() {
        return multipliersList;
    }

    public void setMultipliers(List<String> multipliers) {
        this.multipliersList = multipliers;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BetAmountMultiplier [defaultMultiplierVar=" + defaultMultiplierVar
                + ", multipliersList=" + multipliersList);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}