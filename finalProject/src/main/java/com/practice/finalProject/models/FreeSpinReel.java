package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class FreeSpinReel {

    @XmlAttribute(name = "id")
    private String Id;

    @XmlElement
    private String sValue;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getValue() {
        return sValue;
    }

    public void setValue(String value) {
        this.sValue = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "FreeSpinReel [Id=" + Id + ", sValue=" + sValue);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}