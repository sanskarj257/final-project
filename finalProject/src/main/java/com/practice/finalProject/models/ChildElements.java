package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;

public class ChildElements {

    @XmlAnyElement(lax = true)
    private List<Element> extraProp;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "ChildElements [extraProp=" + extraProp + "]");
        if (extraProp != null) {
            sb.append(", attributes=" + extraProp);
        }
        sb.append("]");
        return sb.toString();
    }
}
