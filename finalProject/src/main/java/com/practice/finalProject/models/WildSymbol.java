package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlElement;

public class WildSymbol {

    @XmlElement(name = "symbol")
    private String symboll;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    public String getSymbol() {
        return symboll;
    }

    public void setSymbol(String symbol) {
        this.symboll = symbol;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "WildSymbol [symboll=" + symboll);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}
