package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
// import jakarta.xml.bind.annotation.XmlRootElement;

public class Game {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement(name = "betAmount")
    private BetAmount betAmount;

    @XmlElement(name = "betAmountMultiplier")
    private BetAmountMultiplier betAmountMultiplier;

    @XmlElement(name = "matrixSize")
    private String matrixSize;

    @XmlElement(name = "winningWay")
    private String winningWay;

    @XmlElementWrapper(name = "symbols")
    @XmlElement(name = "symbol")
    private List<String> symbols;

    @XmlElementWrapper(name = "lines")
    @XmlElement(name = "line")
    private List<Line> lines;

    @XmlElementWrapper(name = "reels")
    @XmlElement(name = "reel")
    private List<Reel> reels;

    @XmlElementWrapper(name = "freeSpinReels")
    @XmlElement(name = "reel")
    private List<FreeSpinReel> freeSpinReels;

    @XmlElement(name = "wildSymbol")
    private WildSymbol wildSymbol;

    @XmlElementWrapper(name = "legendInfo")
    @XmlElement(name = "symbol")
    private List<Symbol> legendInfo;

    @XmlAnyElement(lax = true)
    private List<ChildElements> extraProperties;

    // Getters and setters

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Game [id=" + id + ", betAmount=" + betAmount + ", betAmountMultiplier="
                + betAmountMultiplier
                + ", matrixSize=" + matrixSize + ", winningWay=" + winningWay + ", symbols=" + symbols + ", lines="
                + lines + ", reels=" + reels + ", freeSpinReels=" + freeSpinReels + ", wildSymbol=" + wildSymbol
                + ", legendInfo=" + legendInfo);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }
}