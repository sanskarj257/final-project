package com.practice.finalProject.models;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Line {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement
    private ArrayList<String> value;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "Line [id=" + id + ", value=" + value);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}
