package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Reel {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement
    private String value;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "Reel [id=" + id + ", value=" + value);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}
