package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class BetAmount {

    @XmlAttribute(name = "default")
    private String defaultAmountVar;

    @XmlElement(name = "bet")
    private List<Double> betsList;

    private String defaultAmount;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    public String getDefaultAmount() {
        return defaultAmount;
    }

    public void setDefaultAmount(String defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public List<Double> getBets() {
        return betsList;
    }

    public void setBets(List<Double> bets) {
        this.betsList = bets;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BetAmount [defaultAmountVar=" + defaultAmountVar + ", betsList="
                + betsList + ", defaultAmount=" + defaultAmount);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }
}
