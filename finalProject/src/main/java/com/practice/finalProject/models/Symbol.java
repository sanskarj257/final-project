package com.practice.finalProject.models;

import java.util.List;

import jakarta.xml.bind.Element;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Symbol {

    @XmlAttribute(name = "value")
    private String value;

    @XmlElement(name = "match")
    private List<Match> matches;

    @XmlAnyElement(lax = true)
    private List<Element> extraProperties;

    // Getters and setters

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(
                "Symbol [value=" + value + ", matches=" + matches);
        if (extraProperties != null) {
            sb.append(", extraProperties=" + extraProperties);
        }
        sb.append("]");
        return sb.toString();
    }

}
