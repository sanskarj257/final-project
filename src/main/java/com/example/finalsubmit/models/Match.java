package com.example.finalsubmit.models;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Match {

    @XmlAttribute(name = "type")
    private String type;

    @XmlAttribute(name = "value")
    private String value;

    @XmlElement
    private int count;

    // Getters and setters

    // public String getType() {
    //     return type;
    // }

    // public void setType(String type) {
    //     this.type = type;
    // }

    // public String getValue() {
    //     return value;
    // }

    // public void setValue(String value) {
    //     this.value = value;
    // }

    // public int getCount() {
    //     return count;
    // }

    // public void setCount(int count) {
    //     this.count = count;
    // }

    @Override
    public String toString() {
        return "Match [type=" + type + ", value=" + value + ", count=" + count + "]";
    }
}
