package com.example.finalsubmit.models;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class BetAmount {

    @XmlAttribute(name = "default")
    private String defaultAmount;

    @XmlElement(name = "bet")
    private List<Double> bets;

    // Getters and setters

    // public String getDefaultAmount() {
    //     return defaultAmount;
    // }

    // public void setDefaultAmount(String defaultAmount) {
    //     this.defaultAmount = defaultAmount;
    // }

    // public List<Double> getBets() {
    //     return bets;
    // }

    // public void setBets(List<Double> bets) {
    //     this.bets = bets;
    // }

    @Override
    public String toString() {
        return "BetAmount [defaultAmount=" + defaultAmount + ", bets=" + bets + "]";
    }
    
}
