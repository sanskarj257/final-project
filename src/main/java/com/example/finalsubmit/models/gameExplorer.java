package com.example.finalsubmit.models;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gameExplorer")
@XmlAccessorType(XmlAccessType.FIELD)
public class gameExplorer {

    @XmlElementWrapper(name = "games")
    @XmlElement(name = "game")
    private List<Game> games;

    // Getters and setters

    // public List<Game> getGames() {
    //     return games;
    // }

    // public void setGames(List<Game> games) {
    //     this.games = games;
    // }

    @Override
    public String toString() {
        return "gameExplorer [games=" + games + "]";
    }

}