package com.example.finalsubmit.models;

import jakarta.xml.bind.annotation.XmlElement;

public class WildSymbol {

    @XmlElement(name = "symbol")
    private String symbol;
    
    // Getters and setters

    // public String getSymbol() {
    //     return symbol;
    // }

    // public void setSymbol(String symbol) {
    //     this.symbol = symbol;
    // }

    @Override
    public String toString() {
        return "WildSymbol [symbol=" + symbol + "]";
    }

}
