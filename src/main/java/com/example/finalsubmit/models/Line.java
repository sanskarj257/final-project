package com.example.finalsubmit.models;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Line {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement
    private String value;

    // Getters and setters

    // public String getId() {
    //     return id;
    // }

    // public void setId(String id) {
    //     this.id = id;
    // }

    // public String getValue() {
    //     return value;
    // }

    // public void setValue(String value) {
    //     this.value = value;
    // }

    @Override
    public String toString() {
        return "Line [id=" + id + ", value=" + value + "]";
    }
}
