package com.example.finalsubmit.models;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
// import jakarta.xml.bind.annotation.XmlRootElement;


public class Game {

    @XmlAttribute(name = "id")
    private String id;

    @XmlElement(name = "betAmount")
    private BetAmount betAmount;

    @XmlElement(name = "betAmountMultiplier")
    private BetAmountMultiplier betAmountMultiplier;

    @XmlElement(name = "matrixSize")
    private String matrixSize;

    @XmlElement(name = "winningWay")
    private String winningWay;

    @XmlElementWrapper(name = "symbols")
    @XmlElement(name = "symbol")
    private List<String> symbols;

    @XmlElementWrapper(name = "lines")
    @XmlElement(name = "line")
    private List<Line> lines;

    @XmlElementWrapper(name = "reels")
    @XmlElement(name = "reel")
    private List<Reel> reels;

    @XmlElementWrapper(name = "freeSpinReels")
    @XmlElement(name = "reel")
    private List<FreeSpinReel> freeSpinReels;

    @XmlElement(name = "wildSymbol")
    private WildSymbol wildSymbol;

    @XmlElementWrapper(name = "legendInfo")
    @XmlElement(name = "symbol")
    private List<Symbol> legendInfo;

    // Getters and setters

    // public String getId() {
    //     return id;
    // }

    // public void setId(String id) {
    //     this.id = id;
    // }

    // public BetAmount getBetAmount() {
    //     return betAmount;
    // }

    // public void setBetAmount(BetAmount betAmount) {
    //     this.betAmount = betAmount;
    // }

    // public BetAmountMultiplier getBetAmountMultiplier() {
    //     return betAmountMultiplier;
    // }

    // public void setBetAmountMultiplier(BetAmountMultiplier betAmountMultiplier) {
    //     this.betAmountMultiplier = betAmountMultiplier;
    // }

    // public String getMatrixSize() {
    //     return matrixSize;
    // }

    // public void setMatrixSize(String matrixSize) {
    //     this.matrixSize = matrixSize;
    // }

    // public String getWinningWay() {
    //     return winningWay;
    // }

    // public void setWinningWay(String winningWay) {
    //     this.winningWay = winningWay;
    // }

    // public List<String> getSymbols() {
    //     return symbols;
    // }

    // public void setSymbols(List<String> symbols) {
    //     this.symbols = symbols;
    // }

    // public List<Line> getLines() {
    //     return lines;
    // }

    // public void setLines(List<Line> lines) {
    //     this.lines = lines;
    // }

    // public List<Reel> getReels() {
    //     return reels;
    // }

    // public void setReels(List<Reel> reels) {
    //     this.reels = reels;
    // }

    // public List<FreeSpinReel> getFreeSpinReels() {
    //     return freeSpinReels;
    // }

    // public void setFreeSpinReels(List<FreeSpinReel> freeSpinReels) {
    //     this.freeSpinReels = freeSpinReels;
    // }

    // public WildSymbol getWildSymbol() {
    //     return wildSymbol;
    // }

    // public void setWildSymbol(WildSymbol wildSymbol) {
    //     this.wildSymbol = wildSymbol;
    // }

    // public List<Symbol> getLegendInfo() {
    //     return legendInfo;
    // }

    // public void setLegendInfo(List<Symbol> legendInfo) {
    //     this.legendInfo = legendInfo;
    // }

    @Override
    public String toString() {
        return "Game [id=" + id + ", betAmount=" + betAmount + ", betAmountMultiplier=" + betAmountMultiplier
                + ", matrixSize=" + matrixSize + ", winningWay=" + winningWay + ", symbols=" + symbols + ", lines="
                + lines + ", reels=" + reels + ", freeSpinReels=" + freeSpinReels + ", wildSymbol=" + wildSymbol
                + ", legendInfo=" + legendInfo + "]";
    }

    
}
