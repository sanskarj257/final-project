package com.example.finalsubmit.models;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class Symbol {

    @XmlAttribute(name = "value")
    private String value;

    @XmlElement(name = "match")
    private List<Match> matches;

    // Getters and setters

    // public String getValue() {
    //     return value;
    // }

    // public void setValue(String value) {
    //     this.value = value;
    // }

    // public List<Match> getMatches() {
    //     return matches;
    // }

    // public void setMatches(List<Match> matches) {
    //     this.matches = matches;
    // }

    @Override
    public String toString() {
        return "Symbol [value=" + value + ", matches=" + matches + "]";
    }
}
