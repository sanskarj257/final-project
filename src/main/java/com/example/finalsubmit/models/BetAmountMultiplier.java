package com.example.finalsubmit.models;

import java.util.List;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

public class BetAmountMultiplier {

    @XmlAttribute(name = "default")
    private String defaultMultiplier;

    @XmlElement(name = "multiplier")
    private List<String> multipliers;

    // Getters and setters

    // public String getDefaultMultiplier() {
    //     return defaultMultiplier;
    // }

    // public void setDefaultMultiplier(String defaultMultiplier) {
    //     this.defaultMultiplier = defaultMultiplier;
    // }

    // public List<String> getMultipliers() {
    //     return multipliers;
    // }

    // public void setMultipliers(List<String> multipliers) {
    //     this.multipliers = multipliers;
    // }

    @Override
    public String toString() {
        return "BetAmountMultiplier [defaultMultiplier=" + defaultMultiplier + ", multipliers=" + multipliers + "]";
    }

    
}
