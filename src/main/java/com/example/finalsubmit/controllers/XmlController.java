package com.example.finalsubmit.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.finalsubmit.services.GameService;

import jakarta.xml.bind.JAXBException;

@RestController
@RequestMapping("/api")
public class XmlController {

    @Autowired
    private GameService gameservice;
    

    @PostMapping(value = "/games", consumes = "application/xml")
    public ResponseEntity<Object> saveGame(@RequestBody String xmlData) throws JAXBException, IOException {

        Object gameexplorer = gameservice.XmltoJava(xmlData);
        // System.out.println("\n\n"+gameexplorer);
        // return ResponseEntity.ok(gameexplorer);
        return new ResponseEntity<Object>(gameexplorer, HttpStatus.OK);
    }

}


/* Output
 * 
 * gameExplorer [
 * games=[
 * Game [id=1, betAmount=
 * BetAmount [defaultAmount=1, bets=[0.06, 0.1]], 
 * betAmountMultiplier=
 * BetAmountMultiplier [defaultMultiplier=1, multipliers=[1]], 
 * matrixSize=3,5, 
 * winningWay=LeftToRight, 
 * symbols=[A, B], 
 * lines=[Line [id=1, value=null], 
 * Line [id=2, value=null]], 
 * reels=[Reel [id=1, value=null], 
 * Reel [id=2, value=null]], 
 * freeSpinReels=[
 * FreeSpinReel [id=1, value=null], FreeSpinReel [id=2, value=null]], 
 * wildSymbol=WildSymbol [symbol=W], 
 * legendInfo=[
 * Symbol [value=A, 
 * matches=[Match [type=prize, value=500, count=0], 
 * Match [type=prize, value=250, count=0], 
 * Match [type=prize, value=100, count=0], 
 * Match [type=prize, value=50, count=0]]], 
 * Symbol [value=B, 
 * matches=[
 * Match [type=prize, value=100, count=0], 
 * Match [type=prize, value=50, count=0], 
 * Match [type=prize, value=25, count=0]]]]]]]
 */



//  Input

// <?xml version="1.0" encoding="UTF-8"?>
// <gameExplorer>
//    <games>
//       <game id="1">
//          <betAmount default="1">
//             <bet>0.06</bet>
//             <bet>0.10</bet>
//          </betAmount>
//          <betAmountMultiplier default="1">
//             <multiplier>1</multiplier>
//          </betAmountMultiplier>
//          <matrixSize>3,5</matrixSize>
//          <winningWay>LeftToRight</winningWay>
//          <symbols>
//             <symbol>A</symbol>
//             <symbol>B</symbol>
//          </symbols>
//          <lines default="50">
//             <line id="1">[1,0],[1,1],[1,2],[1,3],[1,4]</line>
//             <line id="2">[0,0],[0,1],[0,2],[0,3],[0,4]</line>
//          </lines>
//          <reels>
//             <reel id="1">[A,15],[B,25],[C,25],[D,25],[E,15],[F,6],[G,15],[H,5],[W,4]</reel>
//             <reel id="2">[A,15],[B,22],[C,22],[D,22],[E,12],[F,8],[G,12],[H,3],[W,4]</reel>
//          </reels>
//          <freeSpinReels>
//             <reel id="1">[A,5],[B,5],[C,10],[D,12],[E,15],[F,15]</reel>
//             <reel id="2">[A,5],[B,5],[C,10],[D,12],[E,15],[F,15]</reel>
//          </freeSpinReels>
//          <legendInfo>     
//    <symbol value="A">
//                <match type="prize" value="500">5</match>
//                <match type="prize" value="250">4</match>
//                <match type="prize" value="100">3</match>
//                <match type="prize" value="50">2</match>
//             </symbol>
//             <symbol value="B">
//                <match type="prize" value="100">5</match>
//                <match type="prize" value="50">4</match>
//                <match type="prize" value="25">3</match>
//             </symbol>
//          </legendInfo>
//          <wildSymbol>
//             <symbol>W</symbol>
//          </wildSymbol>
//       </game>
//    </games>
// </gameExplorer>