package com.example.finalsubmit.services;

import java.io.StringReader;

import org.springframework.stereotype.Service;

import com.example.finalsubmit.models.gameExplorer;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

@Service
public class GameService {
    
    public gameExplorer XmltoJava(String xmlData) throws JAXBException{
        
        
        JAXBContext jaxbContext = JAXBContext.newInstance(gameExplorer.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(xmlData);
        gameExplorer gameexplorer = (gameExplorer) unmarshaller.unmarshal(reader);

        // System.out.println(gameexplorer);
        return gameexplorer;
    }

}
    
    

